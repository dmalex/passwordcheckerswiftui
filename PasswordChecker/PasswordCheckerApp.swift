//
//  PasswordCheckerApp.swift
//  PasswordChecker
//  Created by brfsu on 21.01.2022.
//
import SwiftUI

@main
struct PasswordCheckerApp: App
{
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
