//
//  ContentView.swift
//  PasswordChecker
//  Created by brfsu on 21.01.2022.
//
import SwiftUI

struct ContentView: View
{
    @State var password : String = ""
    @State var passwordStrength : Int = 0
    
    func checkStrength(_ password: String) -> Int {
        let passwordLength = password.count
        var containsSymbol = false
        var containsUppercase = false
        
        for character in password {
            if "ABCDEFGHIJKLMNOPQRSTUVWXYZ".contains(character) {
                containsUppercase = true
            }
            
            if "\"\\ !#$%&’()*+,-./:;<=>?@[]^_’{|}~".contains(character) {
                containsSymbol = true
            }
        }
        
        if passwordLength > 8 && containsSymbol && containsUppercase {
            return 1
        } else {
            return 0
        }
    }
    
    var body: some View {
        VStack {
            Text("How strong is your password?")
                .padding()
            TextField("Enter your password:", text: $password).textFieldStyle(.roundedBorder).frame(width: 250)
            
            if checkStrength(password) ==  0 {
                Text("Weak").foregroundColor(Color.red).font(.system(size: 30)).padding()
            } else {
                Text("Strong").foregroundColor(Color.green).font(.system(size: 30)).padding()
            }
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
